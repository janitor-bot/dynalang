#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

DIR=dynalang-$2

svn export https://dynalang.svn.sourceforge.net/svnroot/dynalang/tags/$2 $DIR

tar cfz dynalang_$2.orig.tar.gz $DIR

rm -rf $DIR ../$2
