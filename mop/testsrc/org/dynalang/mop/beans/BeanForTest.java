/*
   Copyright 2007 Attila Szegedi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.dynalang.mop.beans;

import java.io.Serializable;
import java.util.Arrays;

import junit.framework.AssertionFailedError;

public class BeanForTest
{
    private boolean someBool = true;
    private int someInteger;
    private double someDouble;
    
    public BeanForTest() {
    }

    public BeanForTest(boolean someBool) {
	this.someBool = someBool;
    }
    
    public BeanForTest(int someInteger) {
	this.someInteger = someInteger;
    }
    
    public BeanForTest(double someDouble) {
	this.someDouble = someDouble;
    }
    
    public BeanForTest(boolean someBool, int someInteger) {
	this.someBool = someBool;
	this.someInteger = someInteger;
    }

    public BeanForTest(boolean someBool, double someDouble) {
	this.someBool = someBool;
	this.someDouble = someDouble;
    }
    
    public BeanForTest(int someInteger, double someDouble) {
	this.someInteger = someInteger;
	this.someDouble = someDouble;
    }

    public void setSomeDouble(double someDouble) {
        this.someDouble = someDouble;
    }
    
    public double getSomeDouble() {
        return someDouble;
    }
    
    public void setSomeInteger(int someInt) {
        this.someInteger = someInt;
    }
    
    public int getSomeInteger() {
        return someInteger;
    }
    
    public void setSomeBool(boolean someBool) {
        this.someBool = someBool;
    }
    
    public boolean isSomeBool() {
        return someBool;
    }
    
    public String simpleFixArgMethod(Object o1, Object o2) {
        return o1 + ":" + o2;
    }

    public String simpleVarArgMethod(int i, Object... o) {
        return i + ":" + Arrays.toString(o);
    }

    public String simpleVarArgPrimitiveMethod(int i, int... o) {
        return i + ":" + Arrays.toString(o);
    }
    
    public String overloadedFixArgMethod(int i) {
        return "int:" + i;
    }
    
    public String overloadedFixArgMethod(String s) {
        return "string:" + s;
    }

    public String overloadedFixArgMethod(String s, int i) {
        return "string/int:" + s + i;
    }

    public String overloadedFixArgMethod(int i, String s) {
        return "int/string:" + i + s;
    }
    
    public String overloadedVarArgMethod(int i, int j, String... s) {
	return "int/int/String...:" + i + j + Arrays.toString(s);
    }

    public String overloadedVarArgMethod(int i, String... s) {
	return "int/String...:" + i + Arrays.toString(s);
    }

    public String overloadedAmbiguous(Comparable c) {
	throw new AssertionFailedError(); // Not meant to be called
    }

    public String overloadedAmbiguous(Serializable s) {
	throw new AssertionFailedError(); // Not meant to be called
    }

    public String varArgs(int... is)
    {
        return "varArgs-int...";
    }
    
    public String varArgs(int i1, int i2)
    {
        return "varArgs-int,int";
    }

    public String varArgs(String s, String... ss) {
        /* 
         * This isn't invoked, but it triggers the "Vararg unwrap spec with
         * exactly one parameter more than the current spec influences the 
         * types of the current spec" rule when the varArgs(String) method
         * is processed.
         */
        throw new AssertionFailedError("Not supposed to be called");
    }

    public String varArgs(String s) {
        /* 
         * This isn't invoked ever, but is here only to have a 1-arg fixed 
         * arglen method as well, so when varArgs(1) is invoked, the method
         * map will be forced to rewrap the last argument.
         */
        throw new AssertionFailedError("Not supposed to be called");
    }
    
    public String moreSpecific(String s) {
        return "moreSpecific-String";
    }
    
    public String moreSpecific(Object s) {
        return "moreSpecific-Object";
    }

    public static BeanForTest create() {
	return new BeanForTest();
    }

    public static BeanForTest create(boolean someBool) {
	return new BeanForTest(someBool);
    }
    
    public static BeanForTest create(int someInteger) {
	return new BeanForTest(someInteger);
    }
    
    public static BeanForTest create(double someDouble) {
	return new BeanForTest(someDouble);
    }

    public void create(String string) {
    }
}