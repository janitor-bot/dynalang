package org.dynalang.mop.impl.test;

import junit.framework.TestCase;

import org.dynalang.mop.BaseMetaobjectProtocol;
import org.dynalang.mop.impl.AutoDiscovery;

/**
 * Tests the AutoDiscovery features
 * @author Attila Szegedi
 * @version $Id: $
 */
public class TestAutodiscovery extends TestCase {
    
    public void testAutoDiscovery() throws Exception {
	Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
	BaseMetaobjectProtocol[] mops = 
	    AutoDiscovery.discoverBaseMetaobjectProtocols();
	
	assertNotNull(mops);
	assertEquals(2, mops.length);
	assertNotNull(mops[0]);
	assertSame(DummyMetaobjectProtocol1.class, mops[0].getClass());
	assertNotNull(mops[1]);
	assertSame(DummyMetaobjectProtocol2.class, mops[1].getClass());
    }
}
