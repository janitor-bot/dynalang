/*
   Copyright 2007 Attila Szegedi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.dynalang.mop.beans;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

class Invocation<T extends Member>
{
    private final Object target;
    private final T member;
    private final Object[] args;
    
    Invocation(Object target, T member, Object[] args) {
        this.target = target;
        this.member = member;
        this.args = args;
    }
    
    Object invoke() throws IllegalAccessException, InvocationTargetException,
        InstantiationException
    {
        if(member == null) {
            return OverloadedDynamicMethod.AMBIGUOUS_METHOD;
        }
        return member instanceof Method ? ((Method)member).invoke(target, args)
                : ((Constructor<?>)member).newInstance(args);
    }
}
