package org.dynalang.classtoken;

import java.util.EventListener;

/**
 * Interface that can be implemented in objects interested in receiving events
 * about class tokens.
 * @author Attila Szegedi
 * @version $Id: $
 */
public interface ClassTokenListener extends EventListener {
    
    /**
     * This method is invoked on the listener sometime (no later than next 
     * invocation of {@link ClassToken#forClass(Class)}) after one or more 
     * class tokens become invalid (the classes they represent became less than
     * weakly reachable).
     * @param e the event object describing the event
     */
    public void classTokenInvalidated(ClassTokenInvalidatedEvent e);
}
