package org.dynalang.classtoken;

/**
 * Used as the key for class token canonicalization. Relies on the fact that no
 * two concurrently loaded Class objects will have identical hash code, Class
 * uses Object.hashCode() which says "As much as is reasonably practical, the 
 * hashCode method defined by class <tt>Object</tt> does return distinct 
 * integers for distinct objects. (This is typically implemented by converting 
 * the internal address of the object into an integer, but this implementation
 * technique is not required...)". This would actually not be required at all
 * if only Java would have ConcurrentWeakHashMap.
 * @author Attila Szegedi
 * @version $Id: $
 */
class ClassTokenKey {
    private final String className;
    private final int classHashCode;

    ClassTokenKey(Class<?> clazz) {
	this.className = clazz.getName();
	// java.lang.Class uses Object.hashCode, which is the system identity
	// hash code, so no two concurrently loaded classes will have the same
	// value.
	this.classHashCode = clazz.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
	if(obj instanceof ClassTokenKey) {
	    ClassTokenKey token = (ClassTokenKey)obj;
	    return classHashCode == token.classHashCode && className.equals(
		    token.className);
	}
	return false;
    }
    
    @Override
    public int hashCode() {
        return classHashCode;
    }
}